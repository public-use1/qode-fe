import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <meta name="msapplication-TileColor" content="#da532c" />

          <meta name="application-name" content="APPTEMPLATE" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="default"
          />
          <meta name="apple-mobile-web-app-title" content="APPTEMPLATE" />
          <meta name="description" content="APPTEMPLATE" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />

          <link rel="apple-touch-icon" sizes="180x180" href="#" />
          <link rel="icon" type="image/png" sizes="32x32" href="#" />
          <link rel="icon" type="image/png" sizes="16x16" href="#" />
          <link rel="mask-icon" href="#" color="#5bbad5" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

MyDocument.getInitialProps = async (ctx) => {
  const originalRenderPage = ctx.renderPage;

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: (App) => (props) => <App {...props} />,
    });

  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
  };
};
