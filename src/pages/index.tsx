import Header from '@src/views/components/atoms/Header';
import Home from './Home';

function Index() {
  const title = 'TEST - QODE';
  return (
    <>
      <Header title={title} />
      <Home />
    </>
  );
}

export default Index;
