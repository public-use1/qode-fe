import { Box, Divider, Flex } from "@chakra-ui/react";
import UploadSection from "@src/views/components/molecules/UploadSection";
import ListSection from "@src/views/components/molecules/ListSection";
import { useAppSelector } from "@src/application/hooks";
import ModalComponent from "@src/views/components/atoms/Modal";

const Home = () => {
  const posts = useAppSelector((state) => state.statePost.posts);

  return (
    <>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        flexDirection={"column"}
      >
        <UploadSection />
        <Divider my={5} />
        <Flex direction={"column"} gap={5}>
          {posts.map((el, i) => {
            return <ListSection key={i} id={el.id} image={el.image} />;
          })}
        </Flex>
      </Box>
      <ModalComponent />
    </>
  );
};

export default Home;
