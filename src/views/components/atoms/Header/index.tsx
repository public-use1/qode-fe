import Head from 'next/head';

function Header({
  chilren,
  title = '',
}: {
  chilren?: JSX.Element;
  title?: string;
}): JSX.Element {
  return (
    <Head>
      <title>{title}</title>
      {chilren}
    </Head>
  );
}

export default Header;
