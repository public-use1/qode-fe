import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  OrderedList,
  ListItem,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Divider,
} from '@chakra-ui/react';
import { useAppDispatch, useAppSelector } from '@src/application/hooks';
import { addComment } from '@src/application/slices/stateComment';
import { setIsModalCommentsOpen } from '@src/application/slices/stateModal';
import { useState } from 'react';

function ModalComponent() {
  const dispatch = useAppDispatch();
  const isModalCommentsOpen = useAppSelector(
    (state) => state.stateModal.value.isModalCommentsOpen
  );
  const dataId = useAppSelector((state) => state.stateModal.value.dataId);
  const posts = useAppSelector((state) => state.statePost.posts);
  const allComments = useAppSelector((state) => state.stateComment.comments);
  const [newComment, setnewComment] = useState<string>('');

  const handleCloseModal = () => {
    dispatch(setIsModalCommentsOpen(false));
  };

  const handleAddComments = () => {
    dispatch(
      addComment({
        id: dataId,
        text: newComment,
      })
    );
    setnewComment('');
  };

  return (
    <Modal
      onClose={handleCloseModal}
      isOpen={isModalCommentsOpen}
      scrollBehavior={'inside'}
      isCentered
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Comments</ModalHeader>
        <ModalCloseButton />
        <ModalBody pb={8}>
          <OrderedList>
            {allComments &&
              allComments
                .filter((el) => el.id === dataId)
                .map((el, i) => {
                  return <ListItem key={i}>{el.text}</ListItem>;
                })}
          </OrderedList>
          <Divider my={5} />
          <Flex alignItems="end" justifyContent="center" gap={5}>
            <FormControl>
              <FormLabel>Add Comments</FormLabel>
              <Input
                type="text"
                onChange={(e) => setnewComment(e.target.value)}
                value={newComment}
              />
            </FormControl>
            <Button
              colorScheme="whatsapp"
              //   isDisabled={!selectedFile}
              onClick={handleAddComments}
            >
              Submit
            </Button>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}

export default ModalComponent;
