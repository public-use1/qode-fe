import { Flex, Card, CardFooter, Image, Button } from '@chakra-ui/react';
import { ChatIcon } from '@chakra-ui/icons';
import { useAppDispatch, useAppSelector } from '@src/application/hooks';
import {
  setIsModalCommentsOpen,
  setdataId,
} from '@src/application/slices/stateModal';

interface IListSection {
  id?: number;
  image: string;
}
function ListSection({ id, image }: IListSection) {
  const dispatch = useAppDispatch();
  const handleOpenModalComments = () => {
    dispatch(setdataId(id));
    dispatch(setIsModalCommentsOpen(true));
  };
  return (
    <Flex alignItems="center" justifyContent="center" direction={'column'}>
      <Card maxW="md">
        <Image objectFit="cover" src={image} alt="Chakra UI" />

        <CardFooter
          justify="space-between"
          flexWrap="wrap"
          sx={{
            '& > button': {
              minW: '136px',
            },
          }}
        >
          <Button
            flex="1"
            variant="ghost"
            leftIcon={<ChatIcon />}
            onClick={handleOpenModalComments}
          >
            Comment
          </Button>
        </CardFooter>
      </Card>
    </Flex>
  );
}

export default ListSection;
