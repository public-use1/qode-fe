import { useState, useRef } from "react";
import { Flex, FormControl, FormLabel, Input, Button } from "@chakra-ui/react";
import { useAppDispatch, useAppSelector } from "@src/application/hooks";
import { setPost } from "@src/application/slices/statePost";

function UploadSection() {
  const posts = useAppSelector((state) => state.statePost.posts);

  const dispatch = useAppDispatch();
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [preview, setPreview] = useState("");
  const inputFile = useRef<HTMLInputElement>(null);

  const handleFileInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files ? event.target.files[0] : null;
    setPreview(URL.createObjectURL(file as Blob));
    setSelectedFile(file);
  };

  const handleSubmit = () => {
    dispatch(
      setPost({
        image: URL.createObjectURL(selectedFile as Blob),
      })
    );
    setSelectedFile(null);
    setPreview("");
    if (inputFile.current) {
      inputFile.current.value = "";
      inputFile.current.type = "text";
      inputFile.current.type = "file";
    }
  };

  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      direction={"column"}
      gap={10}
    >
      <Flex alignItems="end" justifyContent="center" gap={5}>
        <FormControl>
          <FormLabel>Upload Picture</FormLabel>
          <Input type="file" onChange={handleFileInput} ref={inputFile} />
        </FormControl>
        <Button
          colorScheme="whatsapp"
          isDisabled={!selectedFile}
          onClick={handleSubmit}
        >
          Submit
        </Button>
      </Flex>
      <FormControl display={"flex"} alignItems="center" justifyContent="center">
        {preview ? <img src={preview} width={244} height={344} alt="thumbnail" /> : ""}
      </FormControl>
    </Flex>
  );
}

export default UploadSection;
