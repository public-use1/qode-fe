import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  value: {
    isModalCommentsOpen: false,
    dataId: 0,
  },
};

export const stateModal = createSlice({
  name: 'stateModal',
  initialState,
  reducers: {
    setIsModalCommentsOpen: (state, action) => {
      state.value.isModalCommentsOpen = action.payload;
    },
    setdataId: (state, action) => {
      state.value.dataId = action.payload;
    },
  },
});

export const { setIsModalCommentsOpen, setdataId } = stateModal.actions;
export default stateModal.reducer;
