import { PayloadAction, createSlice } from '@reduxjs/toolkit';

interface IComments {
  id: number;
  text: string;
}
interface IState {
  comments: IComments[];
}

const initialState: IState = {
  comments: [],
};

export const stateComment = createSlice({
  name: 'stateComment',
  initialState,
  reducers: {
    addComment: (state, action: PayloadAction<IComments>) => {
      state.comments = [...state.comments, action.payload];
    },
  },
});

export const { addComment } = stateComment.actions;
export default stateComment.reducer;
