import { PayloadAction, createSlice } from '@reduxjs/toolkit';

interface IPost {
  id?: number;
  image: string;
}
interface IState {
  posts: IPost[];
}

const initialState: IState = {
  posts: [],
};

let i = 1;
export const statePost = createSlice({
  name: 'statePost',
  initialState,
  reducers: {
    setPost: (state, action: PayloadAction<IPost>) => {
      state.posts = [...state.posts, { id: i++, image: action.payload.image }];
    },
  },
});

export const { setPost } = statePost.actions;
export default statePost.reducer;
