import { configureStore } from '@reduxjs/toolkit';
import statePost from './slices/statePost';
import stateModal from './slices/stateModal';
import stateComment from './slices/stateComment';

export const store = configureStore({
  reducer: {
    statePost,
    stateModal,
    stateComment,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
